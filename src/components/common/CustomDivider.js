import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { verticalScale } from '../../utils';

const styles = {
    dashedStyle: {
        height: 20,
        width: '100%',
        borderWidth: 1,
        // borderStyle: 'dashed',
        borderRadius: 0.1,
        borderColor: 'rgba(33, 33, 33, 0.1)'
    },
    containerStyle: {
        width: '100%',
        height: 1,
        overflow: 'hidden',
        marginTop: verticalScale(10),
        marginBottom: verticalScale(10)
    }
};

const CustomDivider = (props) => {
    return (
        <View
            style={[styles.containerStyle, props.containerStyle]}
        >
            <TouchableOpacity style={[styles.dashedStyle, props.dividerStyle]} />
        </View>
    );
}

export { CustomDivider };