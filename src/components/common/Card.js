import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../utils';

const styles = StyleSheet.create({
  cardContainer: {
    marginLeft: verticalScale(10),
    marginRight: verticalScale(10),
    marginTop: verticalScale(10),
    marginBottom: verticalScale(10),
    position: 'relative',
    padding: verticalScale(10),
    shadowColor: '#333333',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: '#FFFFFF',
    borderRadius: verticalScale(5)
  }
});

const Card = (props) => {
    return (
        <View style={[styles.cardContainer, props.style]}>
            {props.children}
        </View>
    );
}

export { Card };