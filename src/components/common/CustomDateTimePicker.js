import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
    CustomText
} from '../common';
import { verticalScale } from '../../utils';

class CustomDateTimePicker extends Component {
    state = {
        value: this.props.value,
        isPickerVisible: false
    }

    showDateTimePicker() {
        this.setState({ isPickerVisible: true });
    }

    hideDateTimePicker() {
        this.setState({ isPickerVisible: false });
    }
  
    handleDatePicked(value) {
        this.hideDateTimePicker();

        var newValue = '';
        
        if (this.props.mode === 'date') {
            newValue = value.toLocaleDateString();
            this.setState({
                value: newValue
            });
        } else {
            let hour = value.getHours();
            hour = ("0" + hour).slice(-2);

            let minute = value.getMinutes();
            minute = ("0" + minute).slice(-2);

            newValue = hour + ':' + minute;

            this.setState({
                value: newValue
            });
        }
        
        this.props.onValueChange(newValue);
    };
    
    render() {
        return (
            <View style={[styles.containerStyle, this.props.style]}>
                {
                    this.props.label ?
                    <CustomText style={styles.labelStyle}>
                        {this.props.label}
                    </CustomText>
                    :
                    null
                }
                <TouchableOpacity 
                    style={[styles.buttonStyle, this.props.buttonStyle]}
                    onPress={this.showDateTimePicker.bind(this)}
                >
                    <Text style={this.state.value ? styles.buttonTextStyle : styles.buttonTextStyleNull}>
                        {this.state.value || this.props.placeholder}
                    </Text>
                </TouchableOpacity>
                <DateTimePicker
                    mode={this.props.mode}
                    isVisible={this.state.isPickerVisible}
                    onConfirm={this.handleDatePicked.bind(this)}
                    onCancel={this.hideDateTimePicker.bind(this)}
                />
            </View>
        );
    }
};

const styles = {
    pickerStyle: {
        marginLeft: verticalScale(-7),
        marginRight: verticalScale(-7)
    },
    buttonTextStyleNull: {
        color: '#333333',
        opacity: 0.5,
        fontSize: verticalScale(14),
        lineHeight: verticalScale(28),
        height: '100%'
    },
    buttonTextStyle: {
        color: '#333333',
        fontSize: verticalScale(14),
        lineHeight: verticalScale(28),
    },
    buttonStyle: {
        flex: 1,
        paddingHorizontal: verticalScale(5),
    },
    labelStyle: {
        fontSize: verticalScale(10),
    },
    containerStyle: {
        flexDirection: 'column',
        justifyContent: 'center'
    }
};

export { CustomDateTimePicker };