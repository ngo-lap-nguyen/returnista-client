import React, { Component } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import {
    CustomIcon,
    CustomText
} from '../common';
import { verticalScale } from '../../utils';

class CustomSearchbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            focus: false
        }
    }

    render() {
        return (
            <View style={[styles.barStyle, this.props.barStyle]}>
                <CustomIcon
                    name='search'
                    size={verticalScale(14)}
                    style={[
                        styles.iconStyle,
                        this.state.focus ?
                        null
                        :
                        {color: 'rgba(33, 33, 33, 0.3)'}
                    ]}
                />
                <TextInput
                    value={this.state.searchValue}
                    style={styles.inputStyle}
                    onChangeText={(value) => {
                        this.setState({
                            searchValue: value
                        })

                        if (this.props.onChangeText) {
                            this.props.onChangeText(value);
                        }
                    }}
                    placeholder={this.props.placeholder}
                    onSubmitEditing={() => {
                        this.props.onSubmit(this.state.searchValue);
                    }}
                    onFocus={() => {
                        this.setState({
                            focus: true
                        })
                    }}
                />
                {
                    this.state.searchValue ?
                    <TouchableOpacity
                        style={styles.cancelButtonStyle}
                        onPress={() => {
                            this.setState({
                                focus: false,
                                searchValue: ''
                            })
                            Keyboard.dismiss();

                            if (this.props.onCancel) {
                                this.props.onCancel();
                            }
                        }}
                    >
                        <CustomText
                            style={styles.cancelTextStyle}
                        >
                            Hủy
                        </CustomText>
                    </TouchableOpacity>
                    :
                    null
                }
            </View>
        )
    }
}

const styles = {
    barStyle: {
        width: '100%',
        borderRadius: verticalScale(50),
        backgroundColor: '#E8E8E8',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: verticalScale(5)
    },
    iconStyle: {
        position: 'absolute',
        left: verticalScale(10)
    },
    inputStyle: {
        width: '100%',
        padding: 0,
        paddingLeft: verticalScale(30),
        paddingRight: verticalScale(30),
        fontSize: verticalScale(14)
    },
    cancelButtonStyle: {
        position: 'absolute',
        right: verticalScale(10)
    },
    cancelTextStyle: {
        fontSize: verticalScale(14),
        color: '#29ABE2'
    }
};

export { CustomSearchbar };
