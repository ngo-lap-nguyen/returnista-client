import React from 'react';
import { View } from 'react-native';

const Row = (props) => {
    return (
        <View style={[styles.rowStyle, props.style]}>
            {props.children}
        </View>
    );
}

const styles = {
    rowStyle: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export { Row };
