import React from 'react';
import {
    TouchableOpacity
} from 'react-native';
import { verticalScale } from '../../utils';

const CustomButton = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.buttonStyle, props.style]}
        >
            {props.children}
        </TouchableOpacity>
    );
}

const styles = {
    buttonStyle: {
        backgroundColor: '#29ABE2',
        justifyContent: 'center',
        alignItems: 'center',
        padding: verticalScale(10),
        borderRadius: verticalScale(5)
    }
};

export { CustomButton };