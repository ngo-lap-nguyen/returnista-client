import React from 'react';
import { View } from 'react-native';

const Column = (props) => {
    return (
        <View style={[styles.columnStyle, props.style]}>
            {props.children}
        </View>
    );
}

const styles = {
    columnStyle: {
        width: '100%'
    }
};

export { Column };
