import React from 'react';
import { 
    StyleSheet,
    View,
    Modal,
    ScrollView,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    verticalScale
} from '../../utils';

const CustomModal = (props) => {
    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={props.visible}
            onRequestClose={props.onRequestClose}
        >
             <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.3)'
                }}
            >
                <View
                    style={[styles.containerStyle, props.containerStyle]}
                >
                    {
                        props.headerText ?
                        <View>
                            <Text
                                style={styles.headerStyle}
                            >
                                {props.headerText}
                            </Text>
                        </View>
                        :
                        null
                    }
                    <ScrollView
                        flexGrow={0}
                        style={[styles.bodyStyle, props.bodyStyle]}
                    >
                        {props.children}
                    </ScrollView>
                    <View
                        style={styles.footerStyle}
                    >
                        <TouchableOpacity
                            onPress={props.onRequestClose}
                        >
                            <Text
                                style={{fontSize: verticalScale(14), fontWeight: 'bold'}}
                            >
                                QUAY LẠI
                            </Text>
                        </TouchableOpacity>
                        {
                            props.hideConfirm ? 
                            null
                            :
                            <TouchableOpacity
                                onPress={props.onConfirm ? props.onConfirm : props.onRequestClose}
                            >
                                <Text
                                    style={{fontSize: verticalScale(14), fontWeight: 'bold', color: '#29ABE2', marginLeft: verticalScale(15)}}
                                >
                                    XÁC NHẬN
                                </Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '90%',
        backgroundColor: '#FFFFFF',
        position: 'absolute'
    },
    headerStyle: {
        padding: verticalScale(10),
        fontSize: verticalScale(18),
        fontWeight: 'bold',
        color: '#333333',
        borderBottomWidth: 0.5,
        borderColor: 'rgba(33, 33, 33, 0.3)'
    },
    bodyStyle: {
        padding: verticalScale(10),
    },
    footerStyle: {
        borderTopWidth: 0.5,
        borderColor: 'rgba(33, 33, 33, 0.3)',
        padding: verticalScale(10),
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});

export { CustomModal };
