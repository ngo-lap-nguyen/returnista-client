import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Octicons from 'react-native-vector-icons/Octicons';

const CustomIcon = (props) => {
    if (props.type === 'octicons') {
        return (
            <Octicons style={[styles.iconStyle, props.style]} name={props.name} size={props.size}/>
        )
    }

    return (
        <Icon style={[styles.iconStyle, props.style]} name={props.name} size={props.size}/>
    );
}

const styles = {
    iconStyle: {
        color: '#333333',
        maxWidth: '100%'
    }
};

export { CustomIcon };