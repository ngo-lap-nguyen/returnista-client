import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Animated,
} from "react-native";
import Interactable from "react-native-interactable";
import {
    CustomText
} from "../common";
import {
    Screen
} from '../../const';
import {
    verticalScale
} from '../../utils';

class MapPanel extends Component {
    constructor(props) {
        super(props);
        this._deltaY = new Animated.Value(Screen.height - this.props.firstSnapPoint);
    }

    snapTo(params) {
        this.refs.interactable.snapTo(params);
    }
    
    render() {
        return (
            <View style={styles.panelContainer} pointerEvents={"box-none"}>
                <Animated.View
                    pointerEvents={"box-none"}
                    style={[
                        styles.panelContainer,
                        {
                            backgroundColor: "black",
                            opacity: this._deltaY.interpolate({
                                inputRange: [0, Screen.height - this.props.firstSnapPoint],
                                outputRange: [0.5, 0],
                                extrapolateRight: "clamp"
                            })
                        }
                    ]}
                />
                <Interactable.View
                    ref='interactable'
                    verticalOnly={true}
                    snapPoints={[
                        { y: 40 },
                        { y: Screen.height - this.props.firstSnapPoint }
                    ]}
                    boundaries={{ top: -300 }}
                    initialPosition={{ y: Screen.height - this.props.firstSnapPoint }}
                    animatedValueY={this._deltaY}
                >
                    <View style={styles.panel}>
                        <View style={styles.panelHeader}>
                            <View style={styles.panelHandle} />
                        </View>
                        
                        {/* Content */}
                        {this.props.renderHeader()}
                        <View
                            style={{
                                padding: verticalScale(20)
                            }}
                        >
                            {this.props.renderContent ? this.props.renderContent() : null}
                        </View>
                    </View>
                </Interactable.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    panelContainer: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    panel: {
        height: Screen.height + 350,
        backgroundColor: "#FAFAFA",
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.4,
        elevation: 3,
        zIndex: 1000
    },
    panelHeader: {
        alignItems: "center"
    },
    panelHandle: {
        width: verticalScale(40),
        height: verticalScale(5),
        borderRadius: verticalScale(4),
        backgroundColor: "#00000040",
        marginTop: verticalScale(10)
    }
});

export { MapPanel };