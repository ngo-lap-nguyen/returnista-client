import React from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import {
    verticalScale
} from '../../utils';

const styles = StyleSheet.create({
    textStyle: {
        fontSize: verticalScale(14),
        color: "#333333"
    }
});

const CustomText = (props) => {
    return (
        <Text
            style={[styles.textStyle, props.style]}
            onPress={props.onPress}
            numberOfLines={props.numberOfLines}
        >
            {props.children}
        </Text>
    );
}

export { CustomText };
