import React from 'react';
import {
    WebView
} from 'react-native-webview';

const CustomWebView = (props) => {
    return (
        <WebView
            source={{uri: props.uri}}
            style={[props.style, {}]}
        />
    )
}

export { CustomWebView };