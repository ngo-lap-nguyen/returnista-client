import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TextInput,
    Image,
    LayoutAnimation,
    TouchableOpacity
} from 'react-native';
import {
    Card,
    Row,
    CustomText,
} from '../common';
import { connect } from 'react-redux';
import {
    authReducerUpdate,
    authVerifyPhoneNumber,
    authSendSMSToken
} from '../../actions';
import {
    verticalScale,
} from '../../utils';
import {
    fadeAnimationConfig
} from '../../const';

class AuthVerifyPhoneScene extends Component {

    constructor (props) {
        super(props);
        this.state = {
            validationCode: {
                0: '',
                1: '',
                2: '',
                3: ''
            },
            resendValidationCodeCD: 30,
        }

        this.resendValidationCodeCD = null;
    }

    componentWillMount() {
        this.resendValidationCodeInterval = setInterval(() => {
            if (this.state.resendValidationCodeCD > 0) {
                let resendValidationCodeCD = this.state.resendValidationCodeCD - 1;
                this.setState({
                    resendValidationCodeCD
                })
            } else {
                clearInterval(this.resendValidationCodeInterval);
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.resendValidationCodeInterval);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.sendSMS === 'success') {
            this.props.authReducerUpdate('sendSMS', '');
            this.setState({
                resendValidationCodeCD: 30
            }, () => {
                this.resendValidationCodeInterval = setInterval(() => {
                    if (this.state.resendValidationCodeCD > 0) {
                        let resendValidationCodeCD = this.state.resendValidationCodeCD - 1;
                        this.setState({
                            resendValidationCodeCD
                        })
                    } else {
                        clearInterval(this.resendValidationCodeInterval);
                    }
                }, 1000);
            })
        }

        if (nextProps.auth.verifyPhone === 'success') {
            this.props.authReducerUpdate('verifyPhone', '');
            this.props.navigation.navigate('AuthRegister');
        }
    }

    renderValidationInputFields() {
        let inputFields = [0, 0, 0, 0];

        return inputFields.map((val, index) => {
            return (
                <Card
                    key={index}
                    style={{
                        marginLeft: verticalScale(5),
                        marginRight: verticalScale(5),
                        padding: 0,
                        height: verticalScale(45),
                        width: verticalScale(45)
                    }}
                >
                    <Row
                        style={{
                            borderBottomWidth: 0
                        }}
                    >
                        <TextInput
                            autoFocus={index === 0}
                            ref={`validationCodeNo${index}`}
                            value={this.state.validationCode[index]}
                            placeholder={val.toString()}
                            underlineColorAndroid='rgba(0, 0, 0, 0)'
                            keyboardType='numeric'
                            maxLength={1}
                            onChangeText={(value) => {
                                let newValidationCode = { ...this.state.validationCode };
                                newValidationCode[index] = value;

                                this.setState({
                                    validationCode: newValidationCode
                                }, () => {
                                    if (value.length == 0 && index != 0) {
                                        this.refs[`validationCodeNo${index - 1}`].focus();
                                    } else if (value.length != 0 && index != inputFields.length - 1) {
                                        this.refs[`validationCodeNo${index + 1}`].focus();
                                    }
                                })
                            }}
                            style={styles.textInputStyle}
                            onSubmitEditing={(event) => {
                                // Call action here
                                let validationToken = '';
                                let validationCodeValid = true;
                                for (let i = 0; i < 4; i++) {
                                    if (!this.state.validationCode[i]) {
                                        validationCodeValid = false;
                                        break;
                                    } else {
                                        validationToken += this.state.validationCode[i];
                                    }
                                }
                                
                                if (validationCodeValid) {
                                    this.props.authVerifyPhoneNumber(this.props.auth.regPhone, validationToken);
                                }
                            }}
                        />
                    </Row>
                </Card>
            )
        })
    }

    render() {
        return (
            <View
                style={styles.container}
            >
                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: verticalScale(60),
                        paddingRight: verticalScale(60)
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: verticalScale(45)
                        }}
                    >
                        <Image
                            style={{
                                width: verticalScale(100),
                                height: verticalScale(100),
                                resizeMode: 'contain'
                            }}
                            source={require('../../../assets/img/logo-with-text.png')}
                        />
                    </View>
                    {/* Form Section */}
                    <View>
                        <Row
                            style={{
                                paddingBottom: 0
                            }}
                        >
                            <CustomText
                                style={{
                                    textAlign: 'center'
                                }}
                            >
                                Nhập mã xác thực gồm 4 ký tự vào ô bên dưới đây:
                            </CustomText>
                        </Row>
                        <Row
                            style={{
                                paddingVertical: verticalScale(5),
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            {this.renderValidationInputFields()}
                        </Row>
                        <Row
                            style={{
                                paddingVertical: verticalScale(5),
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    // Call action to resend validation code
                                    if (this.state.resendValidationCodeCD === 0) {
                                        this.setState({
                                            validationCode: {
                                                0: '',
                                                1: '',
                                                2: '',
                                                3: ''
                                            }
                                        })

                                        this.props.authSendSMSToken(this.props.auth.regPhone)
                                    } else {
                                        alert('Xin hãy đợi 30s!')
                                    }
                                }}
                            >
                                <CustomText
                                    style={{
                                        textDecorationLine: 'underline',
                                        color: '#4A90E2'
                                    }}
                                >
                                    {
                                        this.state.resendValidationCodeCD ?
                                        `Gửi lại mã xác thực sau (${this.state.resendValidationCodeCD}s)`
                                        :
                                        'Gửi lại mã xác thực'
                                    }
                                </CustomText>
                            </TouchableOpacity>
                        </Row>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#FED001'
    },
    textInputStyle: {
        paddingVertical: verticalScale(5),
        fontSize: verticalScale(14),
        width: '100%',
        textAlign: 'center'
    },
};

const mapStateToProps = ({ auth }) => {
    return {
        auth
    }
}

export default connect(mapStateToProps, {
    authReducerUpdate,
    authVerifyPhoneNumber,
    authSendSMSToken
})(AuthVerifyPhoneScene);