import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TextInput,
    Image,
    LayoutAnimation,
    TouchableOpacity,
    BackHandler
} from 'react-native';
import {
    Card,
    Row,
    CustomText,
    CustomButton,
    CustomIcon
} from '../common';
import { connect } from 'react-redux';
import {
    authReducerUpdate,
    authRegister,
    authLogin
} from '../../actions';
import {
    verticalScale,
} from '../../utils';
import {
    fadeAnimationConfig
} from '../../const';

class AuthRegisterScene extends Component {

    constructor (props) {
        super(props);
        this.state = {
            name: '',
            phone: '',
            password: '',
            secureTextEntry: true,
            email: '',
        }
    }

    componentWillMount() {
        this.setState({
            phone: this.props.auth.regPhone
        })
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.register === 'success') {
            this.props.authReducerUpdate('register', '');
            this.props.authLogin(this.state.phone, this.state.password);
        }
    }

    userInfoValidate() {
        let validationLogString = '';
        let missingFields = [];
        if (this.state.name === '') {
            missingFields.push('Họ tên');
        }

        if (this.state.password === '') {
            missingFields.push('Mật khẩu');
        }

        if (this.state.email === '') {
            missingFields.push('Email');
        }

        if (missingFields.length > 0) {
            let missingFieldString = missingFields[0];
            if (missingFields[1]) {
                missingFieldString += `, ${missingFields[1]}`
            }
            validationLogString += `${missingFieldString} là trường bắt buộc!\n`;
        }

        if (this.state.password.length < 4) {
            validationLogString += 'Mật khẩu phải có ít nhất 4 ký tự!\n';
        }

        if (this.state.email) {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(String(this.state.email).toLowerCase())) {
                validationLogString += 'Địa chỉ email không hợp lệ!'
            }
        }

        if (validationLogString) {
            alert(validationLogString);
            return false;
        }

        return true;
    }

    render() {
        return (
            <View
                style={styles.container}
            >
                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: verticalScale(60),
                        paddingRight: verticalScale(60)
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            paddingTop: verticalScale(45),
                            paddingBottom: verticalScale(15)
                        }}
                    >
                        <Image
                            style={{
                                width: verticalScale(100),
                                height: verticalScale(100),
                                resizeMode: 'contain'
                            }}
                            source={require('../../../assets/img/logo-with-text.png')}
                        />
                    </View>
                    {/* Form Section */}
                    <View>
                        <Row
                            style={{
                                paddingBottom: 0
                            }}
                        >
                            <CustomText
                                style={{
                                    textAlign: 'center'
                                }}
                            >
                                Chúng tôi cam kết bảo mật toàn bộ thông tin cá nhân của bạn và tuân theo quy định luật pháp hiện hành Việt Nam.
                            </CustomText>
                        </Row>
                        <Card
                            style={styles.textInputCardStyle}
                        >
                            <Row
                                style={{paddingBottom: 0}}
                            >
                                <TextInput
                                    autoFocus
                                    value={this.state.name}
                                    style={styles.textInputStyle}
                                    underlineColorAndroid='rgba(0, 0, 0, 0)'
                                    placeholder='Họ và tên'
                                    returnKeyType={'next'}
                                    onSubmitEditing={(event) => this.refs.passwordRef.focus()}
                                    onChangeText={(value) => {
                                        this.setState({
                                            name: value
                                        })
                                    }}
                                />
                                <CustomIcon
                                    name='user'
                                    size={verticalScale(14)}
                                    style={styles.textInputIconStyle}
                                />
                            </Row>
                        </Card>
                        <Card
                            style={styles.textInputCardStyle}
                        >
                            <Row
                                style={{paddingBottom: 0}}
                            >
                                <TextInput
                                    value={this.state.phone}
                                    style={[
                                        styles.textInputStyle,
                                        {
                                            color: '#111111'
                                        }
                                    ]}
                                    underlineColorAndroid='rgba(0, 0, 0, 0)'
                                    returnKeyType={'next'}
                                    editable={false}
                                />
                                <CustomIcon
                                    name='mobile-alt'
                                    size={verticalScale(14)}
                                    style={styles.textInputIconStyle}
                                />
                                <CustomIcon
                                    type='octicons'
                                    name='verified'
                                    size={verticalScale(14)}
                                    style={styles.textInputIconRightStyle}
                                />
                            </Row>
                        </Card>
                        <Card
                            style={styles.textInputCardStyle}
                        >
                            <Row
                                style={{paddingBottom: 0}}
                            >
                                <TextInput
                                    ref='passwordRef'
                                    value={this.state.password}
                                    style={styles.textInputStyle}
                                    underlineColorAndroid='rgba(0, 0, 0, 0)'
                                    returnKeyType={'next'}
                                    onSubmitEditing={(event) => this.refs.emailRef.focus()}
                                    placeholder='Password'
                                    onChangeText={(value) => {
                                        this.setState({
                                            password: value
                                        })
                                    }}
                                    secureTextEntry={this.state.secureTextEntry}
                                />
                                <CustomIcon
                                    name='lock'
                                    size={verticalScale(14)}
                                    style={styles.textInputIconStyle}
                                />
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            secureTextEntry: !this.state.secureTextEntry
                                        })
                                    }}
                                    style={styles.textInputIconRightStyle}
                                >
                                    <CustomIcon
                                        name={this.state.secureTextEntry ? 'eye' : 'eye-slash'}
                                        size={verticalScale(14)}
                                    />
                                </TouchableOpacity>
                            </Row>
                        </Card>
                        <Card
                            style={styles.textInputCardStyle}
                        >
                            <Row
                                style={{paddingBottom: 0}}
                            >
                                <TextInput
                                    ref='emailRef'
                                    value={this.state.email}
                                    style={styles.textInputStyle}
                                    underlineColorAndroid='rgba(0, 0, 0, 0)'
                                    placeholder='Email'
                                    onChangeText={(value) => {
                                        this.setState({
                                            email: value
                                        })
                                    }}
                                />
                                <CustomIcon
                                    name='envelope'
                                    size={verticalScale(14)}
                                    style={styles.textInputIconStyle}
                                />
                            </Row>
                        </Card>
                        <Row
                            style={{
                                marginTop: verticalScale(15),
                                paddingBottom: 0
                            }}
                        >
                            <CustomText
                                style={{
                                    fontSize: verticalScale(11)
                                }}
                            >
                                Chúng tôi sẽ gửi email cho bạn thông tin về tình trạng kiện hàng cùng với thông báo đẩy.
                            </CustomText>
                        </Row>
                        <Card
                            style={{
                                padding: 0,
                                marginLeft: 0,
                                marginRight: 0,
                                marginTop: verticalScale(15),
                                marginBottom: verticalScale(30)
                            }}
                        >
                            <CustomButton
                                onPress={() => {
                                    LayoutAnimation.configureNext(fadeAnimationConfig);

                                    if (this.userInfoValidate()) {
                                        // Call action to register
                                        let userForm = {
                                            phone: this.state.phone,
                                            password: this.state.password,
                                            first_name: this.state.name,
                                            email: this.state.email
                                        }

                                        this.props.authRegister(userForm);
                                    }
                                }}
                                style={{
                                    backgroundColor: '#E83D21'
                                }}
                            >
                                <CustomText
                                    style={{
                                        color: '#FFFFFF',
                                    }}
                                >
                                    Đăng ký
                                </CustomText>
                            </CustomButton>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#FED001'
    },
    textInputStyle: {
        paddingVertical: verticalScale(5),
        fontSize: verticalScale(14),
        width: '100%',
        paddingLeft: verticalScale(30)
    },
    textInputIconStyle: {
        position: 'absolute',
        left: 0,
        width: verticalScale(20),
        textAlign: 'center'
    },
    textInputIconRightStyle: {
        position: 'absolute',
        right: 0,
        width: verticalScale(20),
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInputCardStyle: {
        paddingVertical: 0,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: verticalScale(15)
    }
};

const mapStateToProps = ({ auth }) => {
    return {
        auth
    }
}

export default connect(mapStateToProps, {
    authReducerUpdate,
    authRegister,
    authLogin
})(AuthRegisterScene);