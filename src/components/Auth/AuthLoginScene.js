import _ from 'lodash';
import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    View,
    ScrollView,
    TextInput,
    Alert,
    ActivityIndicator,
    LayoutAnimation,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    Card,
    Row,
    CustomText,
    CustomButton
} from '../common';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import {
    authLogin,
    authGetUser,
    authReducerUpdate,
    notificationRegisterFCM
} from '../../actions';
import {
    verticalScale,
} from '../../utils';
import {
    fadeAnimationConfig
} from '../../const';

class AuthLoginScene extends Component {

    constructor (props) {
        super(props);
        this.state = {
            phone: '',
            password: '',
            contentType: 'sign-in',
            initialLoading: true
        }

        this.loadingTimeout = null;
    }

    retrieveTCMToken = async (userToken) => {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            console.log(fcmToken);
            this.props.notificationRegisterFCM(userToken, fcmToken);
        } else {
            alert('Error: Device token not found')
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh((fcmToken) => {
            // Update new token to server on refresh
            this.props.notificationRegisterFCM(this.props.userToken, fcmToken);
        });

        this.bootstrapAsync();

        this.loadingTimeout = setTimeout(() => {
            LayoutAnimation.configureNext(fadeAnimationConfig);
            this.setState({
                initialLoading: false
            })
        }, 1000);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userToken) {
            this.props.navigation.navigate('Dashboard');
        }
    }

    // Fetch the token from storage then navigate to our appropriate place
    bootstrapAsync = async () => {
        const token = await AsyncStorage.getItem('returnistaClientToken');
        
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        if (token) {
            console.log('User token', token);
            this.props.authReducerUpdate('userToken', token);
            this.props.authGetUser(token);
            this.retrieveTCMToken(token);
            this.props.navigation.navigate('Dashboard');
        } else {
            this.props.navigation.navigate('Auth');
        }
    };

    componentWillUnmount() {
        clearTimeout(this.loadingTimeout);
    }

    render() {
        return (
            <View
                style={styles.container}
            >
                {
                    this.state.initialLoading ?
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <View
                            style={{
                                width: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                                padding: verticalScale(30)
                            }}
                        >
                            <Image
                                style={{
                                    width: verticalScale(100),
                                    height: verticalScale(100),
                                    resizeMode: 'contain'
                                }}
                                source={require('../../../assets/img/logo-with-text.png')}
                            />
                        </View>
                        <ActivityIndicator />
                    </View>
                    :
                    null
                }
                {
                    this.state.initialLoading ?
                    null
                    :
                    <ScrollView
                        contentContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <View
                            style={{
                                width: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                                padding: verticalScale(30)
                            }}
                        >
                            <Image
                                style={{
                                    width: verticalScale(100),
                                    height: verticalScale(100),
                                    resizeMode: 'contain'
                                }}
                                source={require('../../../assets/img/logo-with-text.png')}
                            />
                        </View>
                        <View
            
                        >
                            <Card
                                style={{
                                    marginLeft: verticalScale(60),
                                    marginRight: verticalScale(60),
                                    paddingVertical: 0,
                                }}
                            >
                                <Row
                                    style={{paddingBottom: 0}}
                                >
                                    <TextInput
                                        style={[styles.textInputStyle, {width: '100%'}]}
                                        underlineColorAndroid='rgba(0, 0, 0, 0)'
                                        placeholder='Số điện thoại'
                                        autoCapitalize='none'
                                        onSubmitEditing={(event) => this.refs.passwordInput.focus()}
                                        keyboardType='numeric'
                                        returnKeyType={'next'}
                                        onChangeText={(value) =>
                                            this.setState({
                                                phone: value
                                            })
                                        }
                                    />
                                </Row>
                            </Card>
                            <Card
                                style={{
                                    marginLeft: verticalScale(60),
                                    marginRight: verticalScale(60),
                                    paddingVertical: 0,
                                }}
                            >
                                <Row
                                    style={{paddingBottom: 0}}
                                >
                                    <TextInput
                                        style={[styles.textInputStyle, {width: '60%'}]}
                                        ref='passwordInput'
                                        underlineColorAndroid='rgba(0, 0, 0, 0)'
                                        placeholder='Mật khẩu'
                                        // returnKeyType='go'
                                        // ref={(input) => this.passwordInput = input}
                                        secureTextEntry
                                        onChangeText={(password) => 
                                            this.setState({
                                                password : password
                                            })
                                        }
                                    />
                                    <CustomText
                                        style={{
                                            color : '#FF8383',
                                            position: 'absolute',
                                            right: 0,
                                            fontSize: verticalScale(13)
                                        }}
                                        onPress={() => console.log('Forgot password')}
                                    >
                                        Quên?
                                    </CustomText>
                                </Row>
                            </Card>
                            <Card
                                style={{
                                    padding: 0,
                                    marginLeft: verticalScale(60),
                                    marginRight: verticalScale(60)
                                }}
                            >
                                <CustomButton
                                    onPress={() => {
                                        LayoutAnimation.configureNext(fadeAnimationConfig);
                                        
                                        if (this.state.phone === '' || this.state.password === '') {
                                            alert('Xin hãy nhập đầy đủ SĐT và Mật khẩu!');
                                        } else {
                                            this.props.authLogin(this.state.phone, this.state.password);
                                        }
                                    }}
                                    style={{
                                        backgroundColor: '#E83D21'
                                    }}
                                >
                                    <CustomText
                                        style={{
                                            color: '#FFFFFF',
                                        }}
                                    >
                                        Đăng Nhập
                                    </CustomText>
                                </CustomButton>
                            </Card>
                            {/* <Card
                                style={{
                                    padding: 0,
                                    marginLeft: verticalScale(60),
                                    marginRight: verticalScale(60)
                                }}
                            >
                                <CustomButton
                                    onPress={() => {
                                        LayoutAnimation.configureNext(fadeAnimationConfig);
                                    }}
                                    style={{
                                        backgroundColor: '#3F51B5'
                                    }}
                                >
                                    <CustomIcon
                                        name='facebook-f'
                                        style={{
                                            color: '#FFFFFF',
                                            position: 'absolute',
                                            left: verticalScale(15)
                                        }}
                                    />
                                    <CustomText
                                        style={{
                                            color: '#FFFFFF',
                                        }}
                                    >
                                        Đăng nhập với Facebook
                                    </CustomText>
                                </CustomButton>
                            </Card> */}
                        </View>
                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: verticalScale(5)
                            }}
                            onPress={() => {
                                this.props.navigation.navigate('AuthPhoneInput')
                            }}
                        >
                            <CustomText
                                style={{
                                    textDecorationLine: 'underline',
                                    color: '#4A90E2'
                                }}
                            >
                                Đăng ký với số điện thoại
                            </CustomText>
                        </TouchableOpacity>
                    </ScrollView>
                }
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#FED001'
        // position: 'absolute',
        // bottom: 0
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingVertical: verticalScale(10),
        flexDirection: 'row'
    },
    iconSize: verticalScale(18),
    textInputStyle: {
        paddingVertical: verticalScale(5),
        fontSize: verticalScale(14)
    },
    errorMessage: {
        color: '#ff0000',
        fontSize: verticalScale(14)
    },
};

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.userToken
    }
}

export default connect(mapStateToProps, {
    authLogin,
    authGetUser,
    authReducerUpdate,
    notificationRegisterFCM
})(AuthLoginScene);