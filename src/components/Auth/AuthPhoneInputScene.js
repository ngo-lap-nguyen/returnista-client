import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TextInput,
    Image,
    LayoutAnimation
} from 'react-native';
import {
    Card,
    Row,
    CustomText,
    CustomButton
} from '../common';
import { connect } from 'react-redux';
import {
    authReducerUpdate,
    authSendSMSToken
} from '../../actions';
import {
    verticalScale,
} from '../../utils';
import {
    fadeAnimationConfig
} from '../../const';

class AuthPhoneInputScene extends Component {

    constructor (props) {
        super(props);
        this.state = {
            phone: '',
            areaNumber: '+84'
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.sendSMS === 'success') {
            this.props.authReducerUpdate('sendSMS', '');

            let areaNumber = this.state.areaNumber;
            let phone = this.state.phone;
            if (phone[0] === '0') {
                phone = phone.substr(1);
            }
            this.props.authReducerUpdate('regPhone', areaNumber + phone);

            this.props.navigation.navigate('AuthVerifyPhone');
        }
    }

    render() {
        return (
            <View
                style={styles.container}
            >
                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: verticalScale(60),
                        paddingRight: verticalScale(60)
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: verticalScale(45)
                        }}
                    >
                        <Image
                            style={{
                                width: verticalScale(100),
                                height: verticalScale(100),
                                resizeMode: 'contain'
                            }}
                            source={require('../../../assets/img/logo-with-text.png')}
                        />
                    </View>
                    {/* Form Section */}
                    <View>
                        <Row
                            style={{
                                paddingBottom: 0
                            }}
                        >
                            <CustomText
                                style={{
                                    textAlign: 'center'
                                }}
                            >
                                Chúng tôi cần xác thực số điện thoại cá nhân của bạn để đảm bảo rằng bạn không phải là robot.
                            </CustomText>
                        </Row>
                        <Row
                            style={{
                                paddingVertical: verticalScale(5)
                            }}
                        >
                            <Card
                                style={{
                                    paddingVertical: 0,
                                    marginLeft: 0,
                                    marginRight: verticalScale(15)
                                }}
                            >
                                <Row
                                    style={{paddingBottom: 0}}
                                >
                                    <TextInput
                                        value={this.state.areaNumber}
                                        style={styles.textInputStyle}
                                        underlineColorAndroid='rgba(0, 0, 0, 0)'
                                        placeholder='+84'
                                        autoCapitalize='none'
                                        keyboardType='numeric'
                                        returnKeyType={'next'}
                                        maxLength={3}
                                        onChangeText={(value) => {
                                            if (value.length < 1) {
                                                return;
                                            }

                                            this.setState({
                                                areaNumber: value
                                            })
                                        }}
                                    />
                                </Row>
                            </Card>
                            <Card
                                style={{
                                    paddingVertical: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    flex: 1
                                }}
                            >
                                <Row
                                    style={{paddingBottom: 0}}
                                >
                                    <TextInput
                                        value={this.state.phone}
                                        style={[styles.textInputStyle, {width: '100%'}]}
                                        underlineColorAndroid='rgba(0, 0, 0, 0)'
                                        placeholder='901 23 4567'
                                        autoCapitalize='none'
                                        keyboardType='numeric'
                                        returnKeyType={'next'}
                                        onChangeText={(value) => {
                                            this.setState({
                                                phone: value
                                            })
                                        }}
                                    />
                                </Row>
                            </Card>
                        </Row>
                        <Row
                            style={{
                                paddingBottom: 0
                            }}
                        >
                            <CustomText
                                style={{
                                    fontSize: verticalScale(11)
                                }}
                            >
                                <CustomText
                                    style={{
                                        fontSize: verticalScale(11),
                                        fontWeight: 'bold'
                                    }}
                                >
                                    Lưu ý:&nbsp;
                                </CustomText>
                                Số điện thoại này phải trùng với số điện thoại bạn sử dụng để đặt hàng với TMĐT (e-commerce).
                            </CustomText>
                        </Row>
                        <Card
                            style={{
                                padding: 0,
                                marginLeft: 0,
                                marginRight: 0,
                                marginTop: verticalScale(30)
                            }}
                        >
                            <CustomButton
                                onPress={() => {
                                    LayoutAnimation.configureNext(fadeAnimationConfig);
                                    
                                    if (this.state.phone === '' || this.state.areaNumber.length === 1) {
                                        alert('Xin hãy nhập đầy đủ Mã quốc gia và SĐT trước!')
                                    } else {
                                        this.props.authSendSMSToken(this.state.areaNumber + this.state.phone);
                                    }
                                }}
                                style={{
                                    backgroundColor: '#E83D21'
                                }}
                            >
                                <CustomText
                                    style={{
                                        color: '#FFFFFF',
                                    }}
                                >
                                    Gửi mã xác thực
                                </CustomText>
                            </CustomButton>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#FED001'
    },
    textInputStyle: {
        paddingVertical: verticalScale(5),
        fontSize: verticalScale(14)
    },
};

const mapStateToProps = ({ auth }) => {
    return {
        auth
    }
}

export default connect(mapStateToProps, {
    authReducerUpdate,
    authSendSMSToken
})(AuthPhoneInputScene);