import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import {
    authLogout
} from '../actions';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import {
    CustomText,
    Row
} from './common';
import { verticalScale } from '../utils';

class CustomDrawerContent extends Component {
    render() {
        return (
            <ScrollView
                contentContainerStyle={{
                    flexGrow: 1
                }}
            >
                <View
                    style={{
                        paddingVertical: verticalScale(45),
                        paddingLeft: verticalScale(15),
                        backgroundColor: '#FED001'
                    }}
                >
                    {
                        this.props.user ?
                        <TouchableOpacity
                            onPress={() => {

                            }}
                        >
                            <Row>
                                <Image
                                    style={{
                                        resizeMode: 'cover',
                                        height: verticalScale(64),
                                        width: verticalScale(64),
                                        borderRadius: verticalScale(32)
                                    }}
                                    source={
                                        this.props.user.avatar ?
                                        {uri: this.props.user.avatar}
                                        :
                                        require('../../assets/img/default-avatar.png')
                                    }
                                />
                            </Row>
                            <Row
                                style={{
                                    paddingVertical: verticalScale(10)
                                }}
                            >
                                <CustomText
                                    style={{
                                        fontWeight: 'bold',
                                        fontSize: verticalScale(16),
                                        color: '#FFFFFF'
                                    }}
                                >
                                    {this.props.user.first_name}
                                </CustomText>
                            </Row>
                        </TouchableOpacity>
                        :
                        null
                    }
                </View>
                <SafeAreaView
                    style={styles.containerStyle}
                    forceInset={{
                        top: 'always',
                        horizontal: 'never'
                    }}
                >
                    <View
                        style={{
                            marginTop: 0
                        }}
                    >
                        <DrawerItems {...this.props} />
                        <TouchableOpacity
                            onPress={() => {
                                this.props.authLogout(this.props.userToken);
                            }}
                        >
                            <CustomText
                                style={{
                                    margin: 0,
                                    marginLeft: verticalScale(15),
                                    paddingVertical: verticalScale(10),
                                    fontWeight: 'normal',
                                    fontSize: verticalScale(16),
                                    borderBottomWidth: 0.5,
                                    borderBottomColor: 'rgba(33, 33, 33, 0.3)',
                                    width: '100%',
                                }}
                            >
                                Đăng xuất
                            </CustomText>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
                <View
                    style={{
                        width: '100%',
                        position: 'absolute',
                        bottom: 0,
                        padding: verticalScale(15)
                    }}
                >
                    <Row
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: verticalScale(15)
                        }}
                    >
                        <Image
                            source={require('../../assets/img/logo-without-text.png')}
                            style={{
                                resizeMode: 'contain',
                                height: verticalScale(32),
                                width: verticalScale(32)
                            }}
                        />
                    </Row>
                    <Row
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <CustomText
                            style={{
                                fontSize: verticalScale(10)
                            }}
                        >
                            Bản quyền thuộc về Returnista. Mọi quyền được bảo lưu
                        </CustomText>
                    </Row>
                </View>
            </ScrollView>
        );
    }
};

const styles = {
    containerStyle: {
        flex: 1,
    },
};

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.userToken,
        user: auth.user
    }
}

export default connect(mapStateToProps, {
    authLogout
})(CustomDrawerContent);