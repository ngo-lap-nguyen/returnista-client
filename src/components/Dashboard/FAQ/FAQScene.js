import React from 'react';
import {
    CustomWebView
} from '../../common';

export default FAQScene = (props) => {
    return (
        <CustomWebView 
            uri={'https://intercom.help/returnista-vietnam/vi/'}
        />
    )
}