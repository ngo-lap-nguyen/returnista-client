import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import {
    CustomText,
    CustomIcon,
    CustomModal
} from '../../common';
import {
    verticalScale,
    getMonth,
    formatPrice
} from '../../../utils';
import {
    
} from '../../../actions';
import {
    connect
} from 'react-redux';

class AccountScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            },
            headerRight: (
                <View
                    style={{
                        paddingRight: verticalScale(10),
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                console.log('Add something?')
                            } catch {

                            }
                        }}
                        style={{
                            
                        }}
                    >
                        <CustomText
                            style={{color: '#29ABE2'}}
                        >
                            Options
                        </CustomText>
                    </TouchableOpacity>
                </View>
            ),
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            // ---
            selectedServiceProvisionAction: {
                headerText: '',
                content: '',
                id: -1
            },
            serviceProvisionActionModalVisible: false,
            confirmModalVisible: false,
        }
    }

    componentWillMount() {
        
    }

    componentWillReceiveProps(nextProps) {
        
    }

    renderConfirmModal() {
        return (
            <CustomModal
                headerText={this.state.selectedServiceProvisionAction.headerText}
                visible={this.state.confirmModalVisible}
                onRequestClose={() => {
                    this.setState({
                        selectedServiceProvisionAction: {
                            headerText: '',
                            content: '',
                            id: -1
                        },
                        confirmModalVisible: false
                    })
                }}
                onConfirm={() => {
                    console.log('Selected action', this.state.selectedServiceProvisionAction);
                    this.setState({
                        selectedServiceProvisionAction: {
                            headerText: '',
                            content: '',
                            id: -1
                        },
                        confirmModalVisible: false
                    })
                }}
            >
                <View
                    style={{
                        paddingVertical: verticalScale(15)
                    }}
                >
                    <CustomText>
                        {this.state.selectedServiceProvisionAction.content}
                    </CustomText>
                </View>
            </CustomModal>
        )
    }

    renderServiceProvisionActionModal() {
        return (
            <CustomModal
                visible={this.state.serviceProvisionActionModalVisible}
                onRequestClose={() => 
                    this.setState({
                        serviceProvisionActionModalVisible: false
                    })
                }
                hideConfirm
                bodyStyle={{
                    padding: 0
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            selectedServiceProvisionAction: {
                                headerText: 'Tạm ngưng dịch vụ trong 1h',
                                content: 'Bạn có chắc muốn thực hiện điều này? Dịch vũ sẽ được ngắt kết nối trong 1h trước khi được bật lại.',
                                id: 0
                            },
                            serviceProvisionActionModalVisible: false,
                            confirmModalVisible: true,
                        })
                    }}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: verticalScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: 'rgba(33, 33, 33, 0.3)'
                    }}
                >
                    <CustomText>
                        Tạm ngưng 1h
                    </CustomText>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            selectedServiceProvisionAction: {
                                headerText: 'Tạm ngưng dịch vụ trong 4h',
                                content: 'Bạn có chắc muốn thực hiện điều này? Dịch vũ sẽ được ngắt kết nối trong 4h trước khi được bật lại.',
                                id: 1
                            },
                            serviceProvisionActionModalVisible: false,
                            confirmModalVisible: true,
                        })
                    }}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: verticalScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: 'rgba(33, 33, 33, 0.3)'
                    }}
                >
                    <CustomText>
                        Tạm ngưng 4h
                    </CustomText>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            selectedServiceProvisionAction: {
                                headerText: 'Thông báo đóng cửa hàng',
                                content: 'Bạn có chắc muốn thực hiện điều này?',
                                id: 0
                            },
                            serviceProvisionActionModalVisible: false,
                            confirmModalVisible: true,
                        })
                    }}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: verticalScale(10),
                    }}
                >
                    <CustomText
                        style={{
                            color: '#FF8383'
                        }}
                    >
                        Thông báo đóng cửa hàng
                    </CustomText>
                </TouchableOpacity>
            </CustomModal>
        )
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <View
                    style={{
                        backgroundColor: '#FAFAFA',
                        padding: verticalScale(10),
                        paddingTop: 0,
                        padding: verticalScale(10),
                    }}
                >
                    <CustomText
                        style={{
                            fontSize: verticalScale(24),
                            paddingBottom: verticalScale(10),
                        }}
                    >
                        Tài Khoản
                    </CustomText>
                </View>
                <ScrollView>
                    {/* Partner Info Section */}
                    <CustomText
                        style={{
                            paddingTop: verticalScale(5),
                            paddingRight: verticalScale(10),
                            paddingBottom: verticalScale(5),
                            paddingLeft: verticalScale(10),
                            backgroundColor: '#E8E8E8'
                        }}
                    >
                        Thông tin đối tác
                    </CustomText>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            borderBottomWidth: 0.5,
                            borderColor: '#E8E8E8'
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(5),
                                paddingBottom: verticalScale(5),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Người đại diện
                            </CustomText>
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    color: '#C2C2C2'
                                }}
                                numberOfLines={1}
                            >
                                Nguyễn Hữu Tấn Phong
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            borderBottomWidth: 0.5,
                            borderColor: '#E8E8E8'
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(5),
                                paddingBottom: verticalScale(5),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Ngày có hiệu lực
                            </CustomText>
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    color: '#C2C2C2'
                                }}
                                numberOfLines={1}
                            >
                                12/06/2019
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(5),
                                paddingBottom: verticalScale(5),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Địa chỉ đối tác
                            </CustomText>
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    color: '#C2C2C2'
                                }}
                                numberOfLines={1}
                            >
                                31 Nguyễn Hữu Thọ, Phước Kiến, Nhà Bè
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>

                    {/* Payment Info Section */}
                    <CustomText
                        style={{
                            paddingTop: verticalScale(5),
                            paddingRight: verticalScale(10),
                            paddingBottom: verticalScale(5),
                            paddingLeft: verticalScale(10),
                            backgroundColor: '#E8E8E8'
                        }}
                    >
                        Thông tin thanh toán
                    </CustomText>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            borderBottomWidth: 0.5,
                            borderColor: '#E8E8E8'
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(5),
                                paddingBottom: verticalScale(5),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Phương thức thanh toán
                            </CustomText>
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    color: '#C2C2C2'
                                }}
                                numberOfLines={1}
                            >
                                Chuyển khoản, Tiền mặt
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(5),
                                paddingBottom: verticalScale(5),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Ngày thanh toán
                            </CustomText>
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    color: '#C2C2C2'
                                }}
                                numberOfLines={1}
                            >
                                12/06 hàng tháng
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>

                    {/* Activity Section */}
                    <CustomText
                        style={{
                            paddingTop: verticalScale(5),
                            paddingRight: verticalScale(10),
                            paddingBottom: verticalScale(5),
                            paddingLeft: verticalScale(10),
                            backgroundColor: '#E8E8E8'
                        }}
                    >
                        Hoạt động cửa hàng
                    </CustomText>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                serviceProvisionActionModalVisible: true
                            })
                        }}
                        style={{
                            borderBottomWidth: 0.5,
                            borderColor: '#E8E8E8'
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(10),
                                paddingBottom: verticalScale(10),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Cung cấp dịch vụ
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            borderBottomWidth: 1,
                            borderColor: '#E8E8E8'
                        }}
                    >
                        <View
                            style={{
                                padding: verticalScale(10),
                                paddingTop: verticalScale(10),
                                paddingBottom: verticalScale(10),
                                justifyContent: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                Đăng xuất
                            </CustomText>
                            <CustomIcon
                                name='chevron-right'
                                size={verticalScale(14)}
                                style={{
                                    position: 'absolute',
                                    right: verticalScale(10),
                                    color: '#E8E8E8'
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                </ScrollView>
                {this.renderServiceProvisionActionModal()}
                {this.renderConfirmModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.userToken
    }
}

export default connect(mapStateToProps, {
    
})(AccountScene);