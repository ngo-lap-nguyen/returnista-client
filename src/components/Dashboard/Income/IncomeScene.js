import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    RefreshControl,
    FlatList,
    ScrollView,
} from 'react-native';
import {
    CustomText,
    CustomIcon,
} from '../../common';
import {
    verticalScale,
    getMonth,
    formatPrice
} from '../../../utils';
import {
    
} from '../../../actions';
import {
    connect
} from 'react-redux';

class IncomeScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            }
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        }
    }

    componentWillMount() {
        
    }

    componentWillReceiveProps(nextProps) {
        
    }

    renderDateIncome(data, date) {
        return (
            <View>
                <CustomText
                    style={{
                        paddingTop: verticalScale(5),
                        paddingRight: verticalScale(10),
                        paddingBottom: verticalScale(5),
                        paddingLeft: verticalScale(10),
                        backgroundColor: '#E8E8E8'
                    }}
                >
                    {/* {getDateAndMonth(new Date(date))} */}
                    Hôm nay
                </CustomText>
                <TouchableOpacity
                    onPress={() => {
                        console.log('Click something?')
                    }}
                >
                    <View
                        style={{
                            padding: verticalScale(10),
                            paddingTop: verticalScale(5),
                            paddingBottom: verticalScale(5),
                            justifyContent: 'center'
                        }}
                    >
                        <CustomText
                            style={{
                                fontWeight: 'bold'
                            }}
                        >
                            {formatPrice('190000', 3)}đ
                        </CustomText>
                        <CustomText
                            style={{
                                fontSize: verticalScale(12),
                                color: '#C2C2C2'
                            }}
                        >
                            x{12} kiện hàng
                        </CustomText>
                        <CustomIcon
                            name='chevron-right'
                            size={verticalScale(14)}
                            style={{
                                position: 'absolute',
                                right: verticalScale(10),
                                color: '#E8E8E8'
                            }}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderDateList(income) {
        let dateList = Object.keys(income).map((date) => {
            return {
                date,
                income: income[date]
            }
        })

        return (
            <FlatList
                data={dateList}
                renderItem={this.renderDateIncome}
            />
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                });
            }, 300);
        })
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <View
                    style={{
                        backgroundColor: '#FAFAFA',
                        padding: verticalScale(10),
                        paddingTop: 0,
                        padding: verticalScale(10),
                    }}
                >
                    <CustomText
                        style={{
                            fontSize: verticalScale(24),
                            paddingBottom: verticalScale(10),
                        }}
                    >
                        Thu Nhập
                    </CustomText>
                </View>
                <View
                    style={{
                        padding: verticalScale(30),
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <CustomText
                        style={{
                            fontSize: verticalScale(30),
                            color: '#FF8383',
                        }}
                    >
                        {formatPrice('542000', 3)}đ
                    </CustomText>
                    <CustomText>
                        Tháng 8
                    </CustomText>
                    <CustomText
                        style={{
                            marginTop: verticalScale(15)
                        }}
                    >
                        Thu nhập của bạn:&nbsp;
                        <CustomText
                            style={{
                                fontWeight: 'bold'
                            }}
                        >
                            {formatPrice('150000', 3)}đ
                        </CustomText>
                    </CustomText>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            position: 'absolute',
                            left: verticalScale(10)
                        }}
                    >
                        <CustomIcon name='chevron-left' size={verticalScale(24)} style={{}} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            console.log('Click something?')
                        }}
                        style={{
                            position: 'absolute',
                            right: verticalScale(10)
                        }}
                    >
                        <CustomIcon name='chevron-right' size={verticalScale(24)} style={{}} />
                    </TouchableOpacity>
                </View>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {this.renderDateList({1: 1, 2: 2})}
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.userToken
    }
}

export default connect(mapStateToProps, {
    
})(IncomeScene);