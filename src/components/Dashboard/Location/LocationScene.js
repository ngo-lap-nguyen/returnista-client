import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    RefreshControl,
    LayoutAnimation,
    Image,
    Alert
} from 'react-native';
import {
    DrawerActions
} from 'react-navigation';
import {
    connect
} from 'react-redux';
import MapView, { Marker, Callout } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Polyline from '@mapbox/polyline';
import {
    CustomText,
    CustomIcon,
    MapPanel,
    Row,
    Column
} from '../../common';
import {
    verticalScale,
    requestLocationPermission,
    distance
} from '../../../utils';
import {
    partnerStoreFetch
} from '../../../actions';
import {
    LATITUDE_DELTA,
    LONGITUDE_DELTA,
    Screen,
    SnapPoints,
    fadeAnimationConfig
} from '../../../const';

class LocationScene extends Component {
    static navigationOptions = (props) => {
        return {
            headerLeft: (
                <View
                    style={{
                        paddingLeft: verticalScale(10),
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                props.navigation.dispatch(DrawerActions.openDrawer());
                            } catch {

                            }
                        }}
                    >
                        <CustomText
                            style={{color: '#29ABE2'}}
                        >
                            Menu
                        </CustomText>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            currentPos: {},
            mapRegion: null,
            lastLat: null,
            lastLong: null,
            // Managing display mode
            contentMode: 'guide',
            storeArray: [],
            selectedStore: {},
            nearestStoreId: '',
            // Variables for direction
            directionCoords: [],
            direction: 'false'
        }
    }

    async componentWillMount() {
        this.props.partnerStoreFetch(this.props.userToken);
        await requestLocationPermission();
    }

    // https://stackoverflow.com/questions/43176862/get-current-location-latitude-and-longitude-in-reactnative-using-react-native-m
    componentDidMount() {
        this.getCurrentLocation();
        // this.watchID = navigator.geolocation.watchPosition((position) => {
        //     // Create the object to update this.state.mapRegion through the onRegionChange function
        //     let region = {
        //         latitude: position.coords.latitude,
        //         longitude: position.coords.longitude,
        //         latitudeDelta: LATITUDE_DELTA,
        //         longitudeDelta: LONGITUDE_DELTA
        //     }

        //     this.onRegionChange(region, region.latitude, region.longitude);
        // }, (error) => {
        //     console.log(error);
        // })
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.stores)) {
            let contentMode = 'guide';
            let storeArray = [];

            // If there are selected packages
            if (!_.isEmpty(nextProps.selectedPackages)) {
                let selectedStore = nextProps.stores[nextProps.selectedPackages.store_id];
                storeArray = [ selectedStore ];
                
                if (nextProps.selectedPackages.status === 'Received') {
                    contentMode = 'package-ready';
                } else {
                    contentMode = 'package-collected';
                }

                this.setState({
                    storeArray,
                    contentMode,
                    selectedStore
                }, () => {
                    this.onRegionChange({
                        latitude: selectedStore.coordinate.latitude,
                        longitude: selectedStore.coordinate.longitude
                    })
                })
            } else {
                storeArray = _.values(nextProps.stores);

                this.setState({
                    storeArray,
                    contentMode
                }, () => {
                    this.getNearestStore(this.state.mapRegion);
                })
            }
        }
    }

    mergePos = () => {
        if (!_.isEmpty(this.state.currentPos)) {
            let startConcatPos = this.state.currentPos.latitude + ',' + this.state.currentPos.longitude;
            let destConcatPos = this.state.selectedStore.coordinate.latitude + ',' + this.state.selectedStore.coordinate.longitude;
            
            this.getDirections(startConcatPos, destConcatPos);
            // this.onRegionChange(this.state.currentPos);
        }
    }

    getDirections = async (startPos, destPos) => {
        try {
            let res = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startPos}&destination=${destPos}&key=${'AIzaSyBBqvifbsw8Kg3Zzm7pFCJOLf1MgnvC9vw'}`)
            let resJson = await res.json();
            let points = Polyline.decode(resJson.routes[0].overview_polyline.points);
            let directionCoords = points.map((point, index) => {
                return  {
                    latitude : point[0],
                    longitude : point[1]
                }
            })

            this.setState({
                directionCoords,
                direction: 'true'
            }, () => {
                this.refs.map.fitToElements(true);
            })
        } catch(error) {
            alert(error);
        }
    }

    getNearestStore(curPos) {
        if (_.isEmpty(this.state.storeArray) || _.isEmpty(this.state.mapRegion)) {
            return {};
        }

        let nearestStore = this.state.storeArray[0];
        let nearestDistance = distance(nearestStore.coordinate.latitude, nearestStore.coordinate.longitude, curPos.latitude, curPos.longitude);

        for (let i = 0; i < this.state.storeArray.length; i++) {
            let dist = distance(this.state.storeArray[i].coordinate.latitude, this.state.storeArray[i].coordinate.longitude, curPos.latitude, curPos.longitude);
            if (dist < nearestDistance) {
                nearestStore = this.state.storeArray[i];
                nearestDistance = dist;
            }
        }

        const region = {
            latitude: nearestStore.coordinate.latitude,
            longitude: nearestStore.coordinate.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        };

        this.setState({
            selectedStore: nearestStore,
            nearestStoreId: nearestStore.id
        }, () => {
            this.onRegionChange(region)
        })
    }

    getCurrentLocation() {
        try {
            Geolocation.getCurrentPosition(
                (position) => {
                    const region = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    };

                    this.onRegionChange(region, () => {
                        this.getNearestStore(region);
                    })

                    this.setState({
                        currentPos: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        }
                    }, () => {
                        console.log(this.state.currentPos);
                    })
                },
                (error) => {
                //TODO: better design
                switch (error.code) {
                    case 1:
                        if (Platform.OS === "ios") {
                            Alert.alert(
                                "",
                                "Permission IOS"
                            );
                        } else {
                            Alert.alert(
                                "",
                                "Permission Android"
                            );
                        }
                        break;
                    default:
                        Alert.alert("", "Error getting location!");
                    }
                }
            );
        } catch (error) {
          alert(error.message || "");
        }
    }

    onRegionChange(region, onComplete = null) {
        region.latitudeDelta = LATITUDE_DELTA;
        region.longitudeDelta = LONGITUDE_DELTA;

        this.setState({
            mapRegion: region
        }, () => {
            if (onComplete) {
                onComplete();
            }
        })
    }

    onMapPress(event) {
        let region = {
            latitude: event.nativeEvent.coordinate.latitude,
            longitude: event.nativeEvent.coordinate.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        }

        this.onRegionChange(region);
    }

    onMarkerPress = (store) => {
        this.setState({
            selectedStore: store
        }, () => {
            let region = {
                latitude: store.coordinate.latitude,
                longitude: store.coordinate.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
    
            this.onRegionChange(region);
        })
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                });
            }, 300);
        })
    }

    // Render first-row store info section in panel header
    renderStoreSection() {
        return (
            <Row
                style={{
                    borderBottomWidth: 0.5,
                    borderColor: 'rgba(33, 33, 33, 0.3)',
                    padding: verticalScale(20)
                }}
            >
                <Image
                    style={{
                        width: verticalScale(60),
                        height: verticalScale(60),
                        borderRadius: verticalScale(10)
                    }}
                    source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/7-eleven_logo.svg/1055px-7-eleven_logo.svg.png'}}
                />
                <View
                    style={{
                        paddingLeft: verticalScale(20),
                        flexDirection: 'column',
                        flex: 1
                    }}
                >
                    {/* Name */}
                    <CustomText>
                        {this.state.selectedStore.store_name}
                    </CustomText>

                    {/* Location */}
                    <CustomText
                        style={{
                            fontWeight: 'bold',
                        }}
                    >
                        {this.state.selectedStore.address}
                    </CustomText>

                    {/* Note */}
                    <CustomText
                        style={{
                            color: '#E83D21'
                        }}
                    >
                        {(() => {
                            if (this.state.selectedStore.id === this.state.nearestStoreId) {
                                return 'Gần nhất'
                            }
                        })()}
                    </CustomText>
                </View>
            </Row>
        )
    }

    // Render guide content
    // Content mode: guide-shown
    renderGuideContent() {
        return (
            <View>
                {/* Step 1 */}
                <CustomText
                    style={{
                        fontWeight: 'bold',
                        textDecorationLine: 'underline',
                    }}
                >
                    Bước 1:
                </CustomText>
                <CustomText
                    style={{
                        lineHeight: verticalScale(21)
                    }}
                >
                    - Chọn mục "Địa chỉ thay thế Returnista"{'\n'}
                    - Chọn cửa hàng bất kỳ bạn mong muốn.{'\n'}
                    - Sao chép và dán mục "Địa chỉ nhận hàng" tại sàn thương mại điện tử bạn đang giao dịch.{'\n'}
                </CustomText>

                {/* Step 1 */}
                <CustomText
                    style={{
                        fontWeight: 'bold',
                        textDecorationLine: 'underline',
                    }}
                >
                    Bước 2:
                </CustomText>
                <CustomText
                    style={{
                        lineHeight: verticalScale(21)
                    }}
                >
                    - Giao dịch mua hàng như thông thường.{'\n'}
                    - Ngay khi bạn nhận được thông báo "Kiện hàng đã sẵn sàng" hãy đến cửa hàng bạn đã chọn để nhận hàng trong vòng 15 ngày.{'\n'}
                </CustomText>

                {/* Step 1 */}
                <CustomText
                    style={{
                        fontWeight: 'bold',
                        textDecorationLine: 'underline',
                    }}
                >
                    Bước 3:
                </CustomText>
                <CustomText
                    style={{
                        lineHeight: verticalScale(21)
                    }}
                >
                    - Tại cửa hàng nhận thay thế, bạn cung cấp và thực hiện quét mã nhận hàng với nhân viên.{'\n'}
                    - Nhận hàng và kết thúc giao dịch.{'\n'}
                </CustomText>
            </View>
        )
    }

    // Render second-row action/info section in panel header
    // Content-mode: guide
    renderGuideSection() {
        return (
            <Row
                style={{
                    padding: verticalScale(20)
                }}
            >
                <CustomText
                    style={{
                        width: '80%',
                        fontSize: verticalScale(18),
                        fontWeight: 'bold',
                        color: '#E83D21'
                    }}
                >
                    {
                        this.state.contentMode == 'guide' ?
                        'Tôi muốn cửa hàng này nhận hộ'
                        :
                        'Mua & Quên với Returnista'
                    }
                </CustomText>
                {
                    this.state.contentMode == 'guide' ?
                    <View
                        style={{
                            position: 'absolute',
                            right: verticalScale(20)
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                padding: verticalScale(5),
                                borderRadius: verticalScale(5),
                                borderWidth: 0.5,
                                borderColor: 'rgba(33, 33, 33, 0.3)',
                                backgroundColor: '#FFFFFF'
                            }}
                            onPress={() => {
                                LayoutAnimation.configureNext(fadeAnimationConfig);
                                this.setState({
                                    contentMode: 'guide-shown'
                                })
                            }}
                        >
                            <CustomIcon name='info-circle' size={verticalScale(24)} />
                        </TouchableOpacity>
                    </View>
                    :
                    <Image
                        style={{
                            width: verticalScale(40),
                            height: verticalScale(40),
                            position: 'absolute',
                            right: verticalScale(20)
                        }}
                        source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/7-eleven_logo.svg/1055px-7-eleven_logo.svg.png'}}
                    />
                }
            </Row>
        )
    }

    // Render details of the package
    // Content mode: package-ready-shown
    renderReceivedPackageDetail = () => {
        return (
            <View
                style={{
                    width: '100%',
                    marginTop: verticalScale(20),
                }}
            >
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Số lượng
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.quantity} kiện hàng
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Ngày tiếp nhận
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.date}
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Ngày lưu kho
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.days_in_store} ngày
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Thành tiền
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                fontWeight: 'bold'
                            }}
                        >
                            {this.props.selectedPackages.total_price}đ
                        </CustomText>
                    </Column>
                </Row>
            </View>
        )
    }

    // Render second-row action/info section in panel header
    // Content mode: package-ready-shown
    // Step: 1
    renderBarcodeSection = () => {
        return (
            <View
                style={{
                    width: '100%',
                    marginTop: verticalScale(20)
                }}
            >
                <Row>
                    <Image
                        style={{
                            width: '100%',
                            height: 100,
                            backgroundColor: 'transparent',
                            resizeMode: 'contain'
                        }}
                        source={{
                            uri: `data:image/jpg;base64,${this.props.selectedPackages.barcode}`
                        }}
                    />
                </Row>
                <Row
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <CustomText
                        style={{
                            fontWeight: 'bold',
                            fontSize: verticalScale(24)
                        }}
                    >
                        {this.props.selectedPackages.barcode_content}
                    </CustomText>
                </Row>
            </View>
        )
    }

    // Render second-row action/info section in panel header
    // Content mode: package-ready
    // Step: 0 
    renderPackageReadySection = () => {
        return (
            <View
                style={{
                    padding: verticalScale(20)
                }}
            >
                <Row>
                    <CustomText
                        style={{
                            width: '80%',
                            fontSize: verticalScale(18),
                            fontWeight: 'bold',
                            color: '#E83D21'
                        }}
                    >
                        Kiện hàng đã sẵn sàng
                    </CustomText>
                    <View
                        style={{
                            position: 'absolute',
                            right: 0,
                            flexDirection: 'row'
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                padding: verticalScale(5),
                                borderRadius: verticalScale(5),
                                borderWidth: 0.5,
                                borderColor: 'rgba(33, 33, 33, 0.3)',
                                backgroundColor: '#FFFFFF'
                            }}
                            onPress={() => {
                                this.refs.mapPanel.snapTo({
                                    index: 1
                                })
                                this.mergePos();
                            }}
                        >
                            <CustomIcon name='directions' size={verticalScale(24)} />
                        </TouchableOpacity>
                        {
                            this.state.contentMode === 'package-ready' ?
                            <TouchableOpacity
                                style={{
                                    marginLeft: verticalScale(10),
                                    padding: verticalScale(5),
                                    borderRadius: verticalScale(5),
                                    borderWidth: 0.5,
                                    borderColor: 'rgba(33, 33, 33, 0.3)',
                                    backgroundColor: '#FFFFFF'
                                }}
                                onPress={() => {
                                    LayoutAnimation.configureNext(fadeAnimationConfig);
                                    this.refs.mapPanel.snapTo({
                                        index: 0
                                    })
                                    this.setState({
                                        contentMode: 'package-ready-shown'
                                    })
                                }}
                            >
                                <CustomIcon name='barcode' size={verticalScale(24)} />
                            </TouchableOpacity>
                            :
                            null
                        }
                    </View>
                </Row>
                {
                    this.state.contentMode === 'package-ready-shown' ?
                    <View>
                        {this.renderBarcodeSection()}
                        {this.renderReceivedPackageDetail()}
                    </View>
                    :
                    null
                }
            </View>
        )
    }

    // Render details of the package
    // Content mode: package-collected
    renderCollectedPackageDetail = () => {
        return (
            <View
                style={{
                    width: '100%',
                    marginTop: verticalScale(20),
                }}
            >
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Số lượng
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.quantity} kiện hàng
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Ngày tiếp nhận
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.date}
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Ngày lưu kho
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText>
                            {this.props.selectedPackages.days_in_store} ngày
                        </CustomText>
                    </Column>
                </Row>
                <Row
                    style={{
                        flexWrap: 'wrap',
                        marginBottom: verticalScale(10)
                    }}
                >
                    <Column
                        style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            width: '25%',
                            paddingRight: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                textAlign: 'right'
                            }}
                        >
                            Thành tiền
                        </CustomText>
                    </Column>
                    <Column
                        style={{
                            width: '75%',
                            borderRadius: verticalScale(5),
                            borderWidth: 1,
                            padding: verticalScale(10)
                        }}
                    >
                        <CustomText
                            style={{
                                fontWeight: 'bold'
                            }}
                        >
                            {this.props.selectedPackages.total_price}đ
                        </CustomText>
                    </Column>
                </Row>
            </View>
        )
    }

    // Render second-row action/info section in panel header
    // Content mode: package-collected
    renderPackageCollectedSection = () => {
        return (
            <View
                style={{
                    padding: verticalScale(20)
                }}
            >
                <Row>
                    <CustomText
                        style={{
                            width: '100%',
                            fontSize: verticalScale(18),
                            fontWeight: 'bold',
                            color: '#E83D21'
                        }}
                    >
                        Đã nhận lại kiện hàng
                    </CustomText>
                </Row>
                {this.renderCollectedPackageDetail()}
            </View>
        )
    }

    getFirstSnapPoint() {
        if (this.state.contentMode === 'guide' || this.state.contentMode === 'guide-shown') {
            return SnapPoints.first;
        } else if (this.state.contentMode === 'package-ready-shown') {
            return SnapPoints.second;
        }

        return SnapPoints.first;
    }

    renderLocationPanel = () => {
        return (
            <MapPanel
                ref='mapPanel'
                firstSnapPoint={this.getFirstSnapPoint()}
                renderHeader={() => {
                    return (
                        <View>
                            {/* The store section is static (for the time being) */}
                            {this.renderStoreSection()}
                            
                            {/* The below section depends on contentMode */}
                            {(() => {
                                if (this.state.contentMode == 'guide' || this.state.contentMode == 'guide-shown') {
                                    return this.renderGuideSection();
                                } else if (this.state.contentMode == 'package-ready' || this.state.contentMode == 'package-ready-shown') {
                                    return this.renderPackageReadySection();
                                } else if (this.state.contentMode == 'package-collected') {
                                    return this.renderPackageCollectedSection();
                                }
                            })()}
                        </View>
                    )
                }}
                renderContent={(() => {
                    if (this.state.contentMode == 'guide-shown') {
                        return this.renderGuideContent;
                    }
                })()}
            />
        )
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                {/* Map Part */}
                <View
                    style={{
                        height: Screen.height - SnapPoints.first,
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                    }}
                >
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0
                        }}
                    >
                        <MapView
                            ref='map'
                            style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 0
                            }}
                            region={this.state.mapRegion}
                            showsUserLocation={true}
                            followsUserLocation={true}
                            onPress={this.onMapPress.bind(this)}
                            // onRegionChangeComplete={this.onRegionChange.bind(this)}
                        >
                            {/* Marker for the user's location */}
                            {
                                !_.isEmpty(this.state.currentPos) ?
                                <Marker
                                    coordinate={this.state.currentPos}
                                    title={'Your Location'}
                                />
                                :
                                null
                            }

                            {/* Marker for the polyline of direction */}
                            {
                                !_.isEmpty(this.state.currentPos) && this.state.direction === 'true' ?
                                <MapView.Polyline
                                    coordinates={this.state.directionCoords}
                                    strokeWidth={2}
                                    strokeColor="red"
                                />
                                :
                                null
                            }

                            {/* Markers for all stores in the storeArray */}
                            {(() => {
                                return this.state.storeArray.map((val, index) => {
                                    return (
                                        <Marker
                                            key={index}
                                            coordinate={val.coordinate}
                                            title={val.store_name}
                                            onPress={() => this.onMarkerPress(val)}
                                        >
                                            <Image
                                                source={require('../../../../assets/img/store-pin.png')}
                                                style={{
                                                    width: verticalScale(48),
                                                    height: verticalScale(48)
                                                }}
                                            />
                                        </Marker>
                                    )
                                })
                            })()}
                        </MapView>
                    </View>
                </View>
                {/* Info Part */}
                {this.renderLocationPanel()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: '#F2F2F2'
    }
}

const mapStateToProps = ({ auth, partner, packages }) => {
    return {
        userToken: auth.userToken,
        stores: partner.stores,
        selectedPackages: packages.selectedPackages
    }
}

export default connect(mapStateToProps, {
    partnerStoreFetch
})(LocationScene);