import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    RefreshControl,
    FlatList,
    ScrollView,
    Image
} from 'react-native';
import {
    CustomText,
    CustomIcon,
    Row,
    CustomDivider,
} from '../../common';
import {
    verticalScale,
    getDateAndMonthByString,
} from '../../../utils';
import {
    packageReducerUpdate,
    packageFetch,
    packageFetchSingle
} from '../../../actions';
import {
    connect
} from 'react-redux';

class PackageCollectedScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        }
    }

    componentWillMount() {
        
    }

    componentWillReceiveProps(nextProps) {
        
    }

    componentDidMount() {
        let focusSubscription = this.props.navigation.addListener(
            'willFocus',
            (payload) => {
                this.props.packageFetch(this.props.userToken, 'Collected');
            }
        );

        this.setState({
            focusSubscription
        });
    }

    componentWillUnmount() {
        this.state.focusSubscription.remove();
    }

    renderPackageItem = ({item, index}) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.packageFetchSingle(
                        this.props.userToken,
                        item.store_id,
                        'Collected',
                        item.date,
                        () => {
                            this.props.navigation.navigate('Location');
                        }
                    )
                }}
            >
                <Row>
                    {/* <Image
                        style={{
                            width: verticalScale(50),
                            height: verticalScale(50)
                        }}
                        source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/7-eleven_logo.svg/1055px-7-eleven_logo.svg.png'}}
                    /> */}
                    <CustomText
                        style={{
                            width: '80%',
                            paddingLeft: verticalScale(10)
                        }}
                    >
                        {item.store_name}{'\n'}
                        <CustomText
                            style={{
                                fontSize: verticalScale(12),
                                fontWeight: 'bold'
                            }}
                        >
                            {item.store_address}
                        </CustomText>
                    </CustomText>
                    <CustomText
                        style={{
                            position: 'absolute',
                            right: verticalScale(10),
                            color: '#FF8383',
                        }}   
                    >
                        <CustomIcon name='box' size={verticalScale(16)} />
                    </CustomText>
                </Row>
                {
                    index == this.props.collectedPackages.length ?
                    null
                    :
                    <CustomDivider />
                }
            </TouchableOpacity>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                });
            }, 300);
        })

        this.props.packageFetch(this.props.userToken, 'Collected');
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                    contentContainerStyle={{
                        paddingTop: verticalScale(10)
                    }}
                >
                    <FlatList
                        data={this.props.collectedPackages}
                        renderItem={this.renderPackageItem}
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    }
}

const mapStateToProps = ({ auth, packages }) => {
    return {
        userToken: auth.userToken,
        collectedPackages: packages.collectedPackages
    }
}

export default connect(mapStateToProps, {
    packageReducerUpdate,
    packageFetch,
    packageFetchSingle
})(PackageCollectedScene);