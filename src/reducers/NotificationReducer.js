import _ from 'lodash';
import {
    NOTIFICATION_REDUCER_UPDATE
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,
    fcmToken: ''
}

loadingStack = [];
loadingTimeout = null;

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (NOTIFICATION_REDUCER_UPDATE): {
            // if (action.payload.props === 'loading') {
            //     if (state.loading) {
            //         if (action.payload.value === true) {
            //             loadingStack.push(0);    
            //             return state;
            //         } else {
            //             loadingStack.pop();
            //             console.log(loadingStack);
            //             if (loadingStack.length > 0) {
            //                 return state;
            //             }
            //         }
            //     }
            // }

            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        default: {
            return state;
        }
    }
}