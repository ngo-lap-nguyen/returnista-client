import {
    PARTNER_REDUCER_UPDATE,
    PARTNER_STORE_FETCH
} from '../actions/types';

const INITIAL_STATE = {
    stores: {}
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PARTNER_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case PARTNER_STORE_FETCH: {
            let stores = {};
            
            if (!action.payload.withStatus) {
                stores = { ...state.stores };
            }
            
            for (let i in action.payload.stores) {
                stores[action.payload.stores[i].id] = action.payload.stores[i];
            }

            return {
                ...state,
                stores
            }
        }
        default: {
            return state;
        }
    }
}