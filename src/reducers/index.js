import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import NotificationReducer from './NotificationReducer';
import PartnerReducer from './PartnerReducer';
import PackageReducer from './PackageReducer';

export default combineReducers({
    auth: AuthReducer,
    notification: NotificationReducer,
    partner: PartnerReducer,
    packages: PackageReducer
});