import {
    PACKAGE_REDUCER_UPDATE,
    PACKAGE_FETCH
} from '../actions/types';

const INITIAL_STATE = {
    selectedPackages: {},
    receivedPackages: [],
    collectedPackages: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PACKAGE_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case PACKAGE_FETCH: {
            return {
                ...state
            }
        }
        default: {
            return state;
        }
    }
}