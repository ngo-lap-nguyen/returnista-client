import {
    AUTH_REDUCER_UPDATE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT_SUCCESS
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {
    userToken: '',
    user: null,
    // API flags
    sendSMS: '',
    verifyPhone: '',
    register: '',
    // Register phone no.
    regPhone: ''
}


export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case AUTH_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case AUTH_LOGIN_SUCCESS: {
            AsyncStorage.setItem('returnistaClientToken', action.payload.user.token);

            return {
                ...state,
                userToken: action.payload.user.token,
                user: action.payload.user
            }
        }
        case AUTH_LOGOUT_SUCCESS: {
            return {
                ...state,
                userToken: '',
                user: null
            }
        }
        default: {
            return state;
        }
    }
}