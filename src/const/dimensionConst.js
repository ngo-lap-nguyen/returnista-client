import {
    Dimensions
} from 'react-native';
import { verticalScale } from '../utils';

export const Screen = {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height - 75
};

export const SnapPoints = {
    first: verticalScale(180),
    second: verticalScale(320)
}