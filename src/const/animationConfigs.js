import {
    LayoutAnimation
} from 'react-native';

export const slideAnimationConfig = {
    duration: 100,
    create: {
        type: 'linear',
        property: 'opacity'
    },
    update: {
        type: 'linear'
    },
    delete: {
        type: 'linear',
        property: 'opacity' 
    }
}

export const fadeAnimationConfig = {
    duration: 300,
    create: {
        type: 'linear',
        property: 'opacity'
    },
    update: {
        type: 'linear'
    },
    delete: {
        type: 'linear',
        property: 'opacity' 
    }
}