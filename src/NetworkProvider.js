/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
    BackHandler,
    View,
    ActivityIndicator,
    UIManager
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import AppNavigator from './routes/Navigator';
import { setTopLevelNavigator } from './utils';
import { 
    CustomText,
    CustomModal
} from '../src/components/common';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';

type Props = {};
class NetworkProvider extends Component<Props> {
    constructor(props){
        super(props);
        this.state = {
            isConnected: true
        }
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
        );
    
        NetInfo.isConnected.fetch().done((isConnected) => {    
            if (isConnected) {
                this.setState({isConnected: true});
            } else {
                this.setState({isConnected: false});
            }
        });

        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

        // Build a channel
        const notiChannel = new firebase.notifications.Android.Channel('returnista-client-channel', 'Returnista Client Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('Returnista Client notification channel');

        // Create the channel
        firebase.notifications().android.createChannel(notiChannel);

        // Listener for incoming notification
        this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
            // Process your notification as required
            const localNotification = new firebase.notifications.Notification({
                show_in_foreground: true
            })
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body);
            
            if (Platform.OS === 'android') {
                localNotification
                .android.setChannelId('returnista-client-channel')
                .android.setSmallIcon('ic_stat_ic_notification')
                .android.setPriority(firebase.notifications.Android.Priority.High);
            }
    
            firebase.notifications().displayNotification(localNotification);
        });
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange

        );

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

        this.removeNotificationDisplayedListener();
        this.removeNotificationListener();
    }

    componentWillReceiveProps(nextProps) {
        
    }

    _handleConnectivityChange = (isConnected) => {
        if (isConnected) {
            this.setState({isConnected: true});
        } else {
            this.setState({isConnected: false});
        }
    };

    handleBackButtonClick() {
        BackHandler.exitApp();
        return true;
    }

    render() {        
        return (
            <View
                style={{flex: 1}}
            >
                {
                    this.props.loading ?
                    <View style={styles.loading} pointerEvents={'none'}>
                        <ActivityIndicator color='#29ABE2' animating={true} />  
                    </View>
                    : null
                }
                <AppNavigator
                    ref={(navigatorRef) => {
                        setTopLevelNavigator(navigatorRef);
                    }}
                />
                <CustomModal
                    headerText={'Kết nối với mạng'}
                    visible={!this.state.isConnected}
                    onRequestClose={() => 
                        this.handleBackButtonClick()
                    }
                    hideConfirm
                >
                    <CustomText
                        style={{ color: '#29ABE2', fontStyle: 'italic' }}
                    >
                        Để sử dụng Returnista, hãy bật dữ liệu di động hoặc kết nối Wifi trước.
                    </CustomText>
                </CustomModal>
            </View>
        );        
    }
}

const styles = {
    loading: {
        position: "absolute",
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        alignItems: "center",
        opacity: 0.5,
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        elevation: 999,
        zIndex: 999
    }
}

const mapStateToProps = ({ notification }) => {
    return {
        loading: notification.loading
    }
}

export default connect(mapStateToProps, {

})(NetworkProvider);

