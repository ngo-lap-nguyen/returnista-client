export * from './NavigationUtils';
export * from './ScalingUtils';
export * from './FormatUtils';
export * from './PermissionsUtils';

export const distance = (lat1, lng1, lat2, lng2) => {
    var R = 6371; // km (change this constant to get miles)
	var dLat = (lat2 - lat1) * Math.PI / 180;
	var dLng = (lng2 - lng1) * Math.PI / 180;
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
		Math.sin(dLng / 2) * Math.sin(dLng / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
    
	return d;
}

export const isNumber = (value) => {
    var charCode = value.charCodeAt(0);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}