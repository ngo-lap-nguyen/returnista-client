export const getDateMonthYear = (dateObj) => {
    let year = dateObj.getFullYear();
    let month = dateObj.getMonth() + 1;
    let date = dateObj.getDate();

    return year + '-' + month + '-' + date;
}

export const getDateAndMonth = (dateString) => {
    let curDate = new Date();
    let dateArray = dateString.split('T')[0].split('-');

    let date = dateArray[2];
    let month = dateArray[1];

    if (month == curDate.getMonth() + 1) {
        if (date == curDate.getDate()) {
            return 'Hôm nay';
        } else if (date == curDate.getDate() - 1) {
            return 'Hôm qua';
        }
    }

    return date + '-' + month;
}

export const getMonth = (dateString) => {
    let dateObj = new Date(dateString);

    return 'Tháng ' + dateObj.getMonth() + 1;
}

// ---
export const formatPrice = (value, jump) => {
    if (!jump) {
        jump = 3;
    }

    valueArray = value.toString().split('');
    formattedValue = '';

    count = 0;
    for (i = valueArray.length - 1; i >= 0; i--) {
        count++;
        formattedValue = valueArray[i] + formattedValue;

        if (count == jump && i != 0) {
            formattedValue = '.' + formattedValue;
            count = 0;
        }
    }

    return formattedValue;
}