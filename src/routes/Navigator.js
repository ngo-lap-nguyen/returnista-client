// Controlling routing of the app

import {
    createSwitchNavigator,
    createAppContainer
} from 'react-navigation';

import AuthStack from './Auth/AuthNavigator'
import DashboardDrawer from './Dashboard/DashboardNavigator';

// Implementation of DashboardScene, SignInScene (Auth)
// AuthLoading goes here

const AppNavigator = createSwitchNavigator(
    {   
        Auth: AuthStack,
        Dashboard: DashboardDrawer
    },
    {
        initialRouteName: 'Auth'
    }
);

export default createAppContainer(AppNavigator);