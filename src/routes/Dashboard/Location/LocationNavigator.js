import {
    createStackNavigator,
} from 'react-navigation';
import {
    verticalScale
} from '../../../utils';

import LocationScene from '../../../components/Dashboard/Location/LocationScene';

const LocationStack = createStackNavigator(
    {
        LocationScene: {
            screen: LocationScene
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            }
        }
    }
);

export default LocationStack;
