import React from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import {
    CustomIcon
} from '../../../components/common';
import {
    createMaterialTopTabNavigator,
    createStackNavigator,
} from 'react-navigation';
import PackageReceivedScene from '../../../components/Dashboard/Package/PackageReceivedScene';
import PackageCollectedScene from '../../../components/Dashboard/Package/PackageCollectedScene';
import { verticalScale } from '../../../utils';

const PackageTabs = createMaterialTopTabNavigator(
    {
        PackageReceived: {
            screen: PackageReceivedScene,
            navigationOptions: {
                title: 'Chờ đến nhận'
            }
        },
        PackageCollected: {
            screen: PackageCollectedScene,
            navigationOptions: {
                title: 'Đã nhận'
            }
        }
    },
    {
        tabBarOptions: {
            upperCaseLabel: false,
            labelStyle: {
                color: '#E83D21',
                fontSize: verticalScale(16)
            },
            tabStyle: {
                height: verticalScale(40)
            },
            style: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40)
            },
            indicatorStyle: {
                backgroundColor: '#E83D21'
            }
        }
    }
)

const PackageStack = createStackNavigator(
    {
        Package: {
            screen: PackageTabs,
            navigationOptions: ({navigation}) => {
                return {
                    title: 'Lịch sử Mua & Quên',
                    headerLeft: (
                        <TouchableOpacity
                            style={{
                                paddingLeft: verticalScale(10)
                            }}
                            onPress={() => {
                                navigation.navigate('Location');
                            }}
                        >
                            <CustomIcon name='times' size={14} />
                        </TouchableOpacity>
                    )
                }
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            },
            headerTitleStyle: {
                fontSize: verticalScale(14),
                fontWeight: 'normal'
            }
        },
        headerLayoutPreset: 'center'
    }
)

export default PackageStack;