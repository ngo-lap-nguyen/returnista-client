import {
    createStackNavigator,
} from 'react-navigation';
import IncomeScene from '../../../components/Dashboard/Income/IncomeScene';
import { verticalScale } from '../../../utils';

const IncomeStack = createStackNavigator(
    {
        Income: {
            screen: IncomeScene,
            navigationOptions: {
                title: null
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

export default IncomeStack;