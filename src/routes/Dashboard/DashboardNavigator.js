import React from 'react';
import {
    createDrawerNavigator
} from 'react-navigation';
import {
    verticalScale,
} from '../../utils';
import CustomDrawerContent from '../../components/CustomDrawerContent';

// Import nav stacks
import LocationStack from './Location/LocationNavigator';
import PackageStack from './Package/PackageNavigator';
import FAQStack from './FAQ/FAQNavigator';

const DashboardDrawer = createDrawerNavigator(
    {
        Location: {
            screen: LocationStack,
            // navigationOptions: () => {
            //     return {
            //         drawerLabel: () => null
            //     }
            // }
            navigationOptions: {
                drawerLabel: 'Tất cả Cửa hàng'
            }
        },
        Package: {
            screen: PackageStack,
            navigationOptions: {
                drawerLabel: 'Lịch sử Mua & Quên'
            }
        },
        FAQ: {
            screen: FAQStack,
            navigationOptions: {
                drawerLabel: 'FAQs'
            }
        },
    },
    {
        contentComponent: CustomDrawerContent,
        contentOptions: {
            itemsContainerStyle: {
                paddingVertical: 0,
                paddingHorizontal: 0,
            },
            labelStyle: {
                margin: 0,
                marginLeft: verticalScale(15),
                paddingVertical: verticalScale(10),
                fontWeight: 'normal',
                fontSize: verticalScale(16),
                borderBottomWidth: 0.5,
                borderBottomColor: 'rgba(33, 33, 33, 0.3)',
                width: '100%'
            }
        }
    }
);

export default DashboardDrawer;