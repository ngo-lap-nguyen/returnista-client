import React from 'react';
import {
    TouchableOpacity
} from 'react-native';
import {
    createStackNavigator,
} from 'react-navigation';
import FAQScene from '../../../components/Dashboard/FAQ/FAQScene';
import {
    CustomIcon
} from '../../../components/common';
import { verticalScale } from '../../../utils';

const FAQStack = createStackNavigator(
    {
        Account: {
            screen: FAQScene,
            navigationOptions: ({navigation}) => {
                return {
                    title: 'Câu hỏi thường gặp',
                    headerLeft: (
                        <TouchableOpacity
                            style={{
                                paddingLeft: verticalScale(10)
                            }}
                            onPress={() => {
                                navigation.navigate('Location');
                            }}
                        >
                            <CustomIcon name='times' size={14} />
                        </TouchableOpacity>
                    )
                }
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            },
            headerTitleStyle: {
                fontSize: verticalScale(14),
                fontWeight: 'normal'
            }
        },
        headerLayoutPreset: 'center'
    }
)

export default FAQStack;