import {
    createStackNavigator,
} from 'react-navigation';
import AccountScene from '../../../components/Dashboard/Account/AccountScene';
import { verticalScale } from '../../../utils';

const AccountStack = createStackNavigator(
    {
        Account: {
            screen: AccountScene,
            navigationOptions: {
                title: null
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

export default AccountStack;