import React from 'react';
import {
    TouchableOpacity
} from 'react-native';
import {
    createStackNavigator,
} from 'react-navigation';
import {
    verticalScale
} from '../../utils';
import {
    CustomIcon
} from '../../components/common';

import AuthLoginScene from '../../components/Auth/AuthLoginScene';
import AuthPhoneVerificationStack from './AuthPhoneVerificationStack';
import AuthRegisterScene from '../../components/Auth/AuthRegisterScene';

const AuthStack = createStackNavigator(
    {
        AuthLogin: {
            screen: AuthLoginScene,
            navigationOptions: {
                header: null
            }
        },
        AuthPhoneVerification: {
            screen: AuthPhoneVerificationStack,
            navigationOptions: {
                header: null
            }
        },
        AuthRegister: {
            screen: AuthRegisterScene,
            navigationOptions: ({navigation}) => {
                return {
                    title: 'Đăng ký tài khoản',
                    headerLeft: (
                        <TouchableOpacity
                            style={{
                                paddingLeft: verticalScale(10)
                            }}
                            onPress={() => {
                                navigation.navigate('AuthLogin');
                            }}
                        >
                            <CustomIcon name='times' size={14} />
                        </TouchableOpacity>
                    )
                }
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            },
            headerTitleStyle: {
                fontSize: verticalScale(14),
                fontWeight: 'normal'
            }
        },
        headerLayoutPreset: 'center'
    }
);

export default AuthStack;
