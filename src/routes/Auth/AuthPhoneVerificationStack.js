import React from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import {
    CustomIcon
} from '../../components/common';
import {
    createStackNavigator,
} from 'react-navigation';
import { verticalScale } from '../../utils';

import AuthPhoneInputScene from '../../components/Auth/AuthPhoneInputScene';
import AuthVerifyPhoneScene from '../../components/Auth/AuthVerifyPhoneScene';

const AuthPhoneVerificationStack = createStackNavigator(
    {
        AuthPhoneInput: {
            screen: AuthPhoneInputScene,
            navigationOptions: ({navigation}) => {
                return {
                    title: 'Xác thực số điện thoại',
                    headerLeft: (
                        <TouchableOpacity
                            style={{
                                paddingLeft: verticalScale(10)
                            }}
                            onPress={() => {
                                navigation.navigate('AuthLogin');
                            }}
                        >
                            <CustomIcon name='times' size={14} />
                        </TouchableOpacity>
                    )
                }
            }
        },
        AuthVerifyPhone: {
            screen: AuthVerifyPhoneScene,
            navigationOptions: {
                title: 'Xác thực số điện thoại'
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#FAFAFA',
                height: verticalScale(40),
                shadowOpacity: 0,
                elevation: 0
            },
            headerTitleStyle: {
                fontSize: verticalScale(14),
                fontWeight: 'normal'
            }
        },
        headerLayoutPreset: 'center'
    }
)

export default AuthPhoneVerificationStack;