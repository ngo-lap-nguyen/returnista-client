import {
    AUTH_REDUCER_UPDATE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT_SUCCESS
} from './types';
import {
    notificationReducerUpdate
} from '../actions';
import AsyncStorage from '@react-native-community/async-storage';
import { navigate } from '../utils';

export const authReducerUpdate = (props, value) => {
    return {
        type: AUTH_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const authLogin = (username, password) => {
    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch('https://api.returnista.co/api/v1/auth/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username,
                    password
                })
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    let user = results.results.user;
                    user.token = results.results.token;

                    dispatch({
                        type: AUTH_LOGIN_SUCCESS,
                        payload: {
                            user
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authLogout = (userToken) => {
    return async (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch('https://api.returnista.co/api/v1/auth/logout', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    dispatch({
                        type: AUTH_LOGOUT_SUCCESS,
                        payload: null
                    })

                    (async () => {
                        await AsyncStorage.removeItem('returnistaClientToken');
                    })();
                    navigate('Auth');
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authSendSMSToken = (phone) => {
    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return  fetch('https://api.returnista.co/api/v1/auth/send-sms-token', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: phone
                })
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    dispatch({
                        type: AUTH_REDUCER_UPDATE,
                        payload: {
                            props: 'sendSMS',
                            value: 'success'
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authVerifyPhoneNumber = (phone, token) => {
    let tokenString = token[0] + token[1] + token[2] + token[3];

    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return  fetch('https://api.returnista.co/api/v1/auth/verify-phone-number', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: phone,
                    token: tokenString
                })
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    dispatch({
                        type: AUTH_REDUCER_UPDATE,
                        payload: {
                            props: 'verifyPhone',
                            value: 'success'
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authRegister = (userForm) => {
    return (dispatch) => { 
        dispatch(notificationReducerUpdate('loading', true));
        return  fetch('https://api.returnista.co/api/v1/auth/register', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userForm)
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    dispatch({
                        type: AUTH_REDUCER_UPDATE,
                        payload: {
                            props: 'register',
                            value: 'success'
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authGetUser = (userToken) => {
    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch(`https://api.returnista.co/api/v1/users/info`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.id) {
                    let user = results.results ? results.results : results;
                    user.token = userToken;

                    dispatch({
                        type: AUTH_REDUCER_UPDATE,
                        payload: {
                            props: 'user',
                            value: user
                        }
                    })
                } else if (results.code === 1002) {
                    alert('Có lỗi xảy ra. Vui lòng đăng nhập lại.');
                    dispatch({
                        type: AUTH_LOGOUT_SUCCESS,
                        payload: null
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authUserInfoUpdate = (userToken, userForm) => {
    return (dispatch) => { 
        dispatch(notificationReducerUpdate('loading', true));
        return  fetch('https://api.returnista.co/api/v1/users', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(userForm)
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.id) {
                    let user = results.results ? results.results : results;
                    user.token = userToken;

                    dispatch({
                        type: AUTH_REDUCER_UPDATE,
                        payload: {
                            props: 'user',
                            value: user
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const authUploadAvatar = (userToken, image) => {
    let formData = new FormData();
    formData.append('image', image);

    return (dispatch) => { 
        dispatch(notificationReducerUpdate('loading', true));
        return  fetch('https://api.returnista.co/api/v1/users/upload-image', {
                method: 'POST',
                headers: {
                    'Authorization': `Token ${userToken}`
                },
                body: formData
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    let avatarUrl = results.results.url;
                    
                    dispatch(authUserInfoUpdate(userToken, {
                        avatar: avatarUrl
                    }))
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}