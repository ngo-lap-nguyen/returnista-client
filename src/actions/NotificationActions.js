import {
    NOTIFICATION_REDUCER_UPDATE
} from './types';

export const notificationReducerUpdate = (props, value) => {
    return {
        type: NOTIFICATION_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const notificationRegisterFCM = (userToken, fcmToken) => {
    return (dispatch) => {
        return fetch('https://api.returnista.co/api/v1/notification/register_device/gcm', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    registration_id: fcmToken
                })
            })
            .then((res) => {
                console.log('API Res', res);
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.results === 200) {
                    dispatch({
                        type: NOTIFICATION_REDUCER_UPDATE,
                        payload: {
                            props: 'fcmToken',
                            value: fcmToken
                        }
                    })
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}