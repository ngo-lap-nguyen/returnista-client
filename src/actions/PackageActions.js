import {
    PACKAGE_REDUCER_UPDATE,
    PACKAGE_FETCH
} from './types';
import {
    notificationReducerUpdate
} from '.';

export const packageReducerUpdate = (props, value) => {
    return {
        type: PACKAGE_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const packageFetch = (userToken, packageStatus, date = null) => {
    let apiUrl = `https://api.returnista.co/api/v1/partner/client-history-packages?status=${packageStatus}`;
    if (date) {
        apiUrl += `&date=${date}`;
    }

    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch(apiUrl, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.count) {
                    let packages = results.results;

                    if (packageStatus === 'Received') {
                        dispatch({
                            type: PACKAGE_REDUCER_UPDATE,
                            payload: {
                                props: 'receivedPackages',
                                value: packages
                            }
                        })
                    } else {
                        dispatch({
                            type: PACKAGE_REDUCER_UPDATE,
                            payload: {
                                props: 'collectedPackages',
                                value: packages
                            }
                        })
                    }
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const packageFetchSingle = (userToken, storeId, packageStatus, date, onComplete = null) => {
    let apiUrl = `https://api.returnista.co/api/v1/partner/client-packages`;
    if (date) {
        apiUrl += `?date=${date}`;
    }

    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    store: storeId,
                    status: packageStatus
                })
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200) {
                    let selectedPackages = results.results;
                    selectedPackages.status = packageStatus;
                    selectedPackages.store_id = storeId;

                    dispatch({
                        type: PACKAGE_REDUCER_UPDATE,
                        payload: {
                            props: 'selectedPackages',
                            value: selectedPackages
                        }
                    })

                    if (onComplete) {
                        onComplete();
                    }
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}