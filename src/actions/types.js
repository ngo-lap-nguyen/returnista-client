// Auth types
export const AUTH_REDUCER_UPDATE = 'auth_reducer_update';
export const AUTH_LOGIN_SUCCESS = 'auth_login_success';
export const AUTH_LOGOUT_SUCCESS = 'auth_logout_success';

// Notification types
export const NOTIFICATION_REDUCER_UPDATE = 'notification_reducer_update';
export const NOTIFICATION_REGISTER_FCM = 'notification_register_fcm';

// Partner types
export const PARTNER_REDUCER_UPDATE = 'partner_reducer_update';
export const PARTNER_STORE_FETCH = 'package_partner_store_fetch';

// Package types
export const PACKAGE_REDUCER_UPDATE = 'package_reducer_update';
export const PACKAGE_FETCH = 'package_fetch';