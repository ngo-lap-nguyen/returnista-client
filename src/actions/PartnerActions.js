import {
    PARTNER_REDUCER_UPDATE,
    PARTNER_STORE_FETCH
} from './types';
import {
    getDateMonthYear,
    renameKey
} from '../utils';
import {
    notificationReducerUpdate
} from '.';

export const partnerReducerUpdate = (props, value) => {
    return {
        type: PARTNER_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const partnerStoreFetch = (userToken, status = null) => {
    let apiUrl = 'https://api.returnista.co/api/v1/partner/store';
    if (status) {
        apiUrl += `?package_status=${status}`;
    }

    return (dispatch) => {
        dispatch(notificationReducerUpdate('loading', true));
        return fetch(apiUrl, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((res) => {
                console.log('API Res', res);
                dispatch(notificationReducerUpdate('loading', false));
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.count >= 0) {
                    let stores = results.results;

                    dispatch({
                        type: PARTNER_STORE_FETCH,
                        payload: {
                            stores,
                            withStatus: status ? true : false
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}